const matches= require('../data/matches.json');
const deliveries = require('../data/deliveries.json')

// 1.Number of matches played per year for all the years in IPL.

const MatchesPerYear =(matches)=>{

    let totalMatchesYearly = {}

    for(let i=0; i<matches.length; i++){

        if(!totalMatchesYearly[matches[i].season]){     // year not exist assign count 1

            totalMatchesYearly[matches[i].season] = 1;
        }

        else{

            totalMatchesYearly[matches[i].season]+=1    // else increment count of that 0year
        }
    }

    return totalMatchesYearly
};

// 2.Number of matches won per team per year in IPL.

const WinnerTeamYearly = (matches)=>{

    let WinnerData = {};
    
    for(let j=0; j<matches.length; j++){

        if(!WinnerData.hasOwnProperty(matches[j].season)){

            WinnerData[matches[j].season] = {}
        
            if(WinnerData[matches[j].season].hasOwnProperty(matches[j].winner)){

                WinnerData[matches[j].season][(matches[j].winner)]++
            }

            else{

                let Winnerteam = {}

                Winnerteam[matches[j].winner] = 1

                Object.assign(WinnerData[matches[j].season],Winnerteam)
            }
        }
        else{

            if(WinnerData[matches[j].season].hasOwnProperty(matches[j].winner)){

                WinnerData[matches[j].season][(matches[j].winner)]++
            }

            else{

                let Winnerteam = {}

                Winnerteam[matches[j].winner] = 1

                Object.assign(WinnerData[matches[j].season],Winnerteam)
            }
        }
             
    }
   
    return WinnerData
}


// 3.Extra runs conceded per team in the year 2016.

const ExtraRunsByTeam =(deliveries,matches,year)=>{

    let extraRunTeam = {}

    // const matchID= (matches.filter((match)=>match.season==year)).map((matchData)=>matchData.id) //collect ID's of data  of that year
    const matchID =matches.reduce((matchIDdata,curntData)=>{
        if(curntData.season==year){
            matchIDdata.add(curntData.id)
        }
        return matchIDdata
    },new Set())
   
    for(let i=0; i< deliveries.length; i++){

        if(matchID.has(deliveries[i].match_id)){

            if(extraRunTeam.hasOwnProperty(deliveries[i].bowling_team)){

                extraRunTeam[deliveries[i].bowling_team] += Number(deliveries[i].extra_runs)
            }
            else{

                extraRunTeam[deliveries[i].bowling_team] = Number(deliveries[i].extra_runs)
            }
        }
    }

    return extraRunTeam
}

// 4.Top 10 economical bowlers in the year 2015.

const TopTenEconomicalbowlers =(deliveries,matches,year)=>{

    let bowlersData = {}

    let BowlersEconomy = {}

    // const matchID = (matches.filter((match)=>match.season==year)).map((matchData)=>matchData.id)
    
    // storing Conceded runs and balls balled data of baller 

    const matchID =matches.reduce((matchIDdata,curntData)=>{

        if(curntData.season==year){

            matchIDdata.add(curntData.id)
        }
        
        return matchIDdata
    },new Set());

    for(let i=0; i< deliveries.length; i++){

        if(matchID.has(deliveries[i].match_id)){

            let ballRuns = deliveries[i].total_runs

            if(bowlersData.hasOwnProperty(deliveries[i].bowler)){

                bowlersData[deliveries[i].bowler]["ConcededRuns"]  += Number(ballRuns);
                bowlersData[deliveries[i].bowler]["NoOfBalls"] += 1;
            }
            else{

                bowlersData[deliveries[i].bowler] = {

                    "ConcededRuns" : Number(ballRuns),
                    "NoOfBalls": 1

                }
            }
        }
    }

    // finding economic of baller for each 

    for(let ballerName in bowlersData){

        
        BowlersEconomy[ballerName] = (bowlersData[ballerName]["ConcededRuns"] / bowlersData[ballerName]["NoOfBalls"]) * 6;
    }

    //sorting top 10 bowlers 

    const result = Object.entries(BowlersEconomy).sort((a,b) => a[1]-b[1])
    
    return result.slice(0,10);

}


//5. Find the number of times each team won the toss and also won the match.

const NoOfTimesTossAndMatchWins = (matches)=>{

    
    let TeamNamesObject = matches.reduce((TeamNames,data)=>{

        
        if (!TeamNames.hasOwnProperty(data.team1)){
            
            Object.assign(TeamNames,{[data.team1]:0})
        }
        if (!TeamNames.hasOwnProperty(data.team2)){

            Object.assign(TeamNames,{[data.team2]:0})
        }

        else{
            
            if(data.toss_winner==data.winner){

            TeamNames[data.winner] += 1
        }
        
        }

        return TeamNames
    },{});

     return TeamNamesObject  
}

//6. Find a player who has won the highest number of Player of the Match awards for each season

const HighestNumberOfManOfTheMatchPlayer = (matches)=>{

    let Players = matches.reduce((PlayerData,data)=>{

        if(PlayerData.hasOwnProperty(data.season)){

            if(PlayerData[data.season].hasOwnProperty(data.player_of_match)){

                PlayerData[data.season][data.player_of_match] += 1;
            }
            else{

                PlayerData[data.season][data.player_of_match] = 1;
            }
    }
    else{

        PlayerData[data.season] = {}
    }

        return PlayerData
    },{});

    // sortting player name by Object values

    let result = Object.keys(Players).reduce((sortedData,data)=>{

    if(!sortedData.hasOwnProperty(data)){

        sortedData[data] = []

        sortedData[data] =(Object.entries(Players[data]).sort((a,b) => b[1]-a[1]))
    }
    
    return sortedData

    },{});

    // Highest player with awards in each season  

    let finalResult = Object.keys(result).reduce((highestPlayers,dataArr)=>{
        
        
        highestPlayers[dataArr] = result[dataArr].filter((data)=>data[1]==result[dataArr][0][1])
    
        return highestPlayers
    
    },{});

    return finalResult   
}


//7.Find the strike rate of a batsman for each season

    /*format {
             2008:{butler: 141, stroke : 168},
             2009:{ben_strokes : 189, chris : 228}
          }
          
          Strike Rate = ((Runs scored)×100)/Balls faced */


const StrikeRateOfBatsmanBySeason =(deliveries,matches)=>{

    //season data ID & Year 
    let seasonData = matches.reduce((seasonData,data)=>{
        
        if(seasonData.hasOwnProperty(data.season)){
            
            seasonData[data.season].add(data.id)}
        else{

            seasonData[data.season] = new Set()
            seasonData[data.season].add(data.id)
        }
        
        return seasonData   

    },{});
   
    let StrikeRateData = {}
    

    for(year in seasonData){ 
        
       
        if(!StrikeRateData.hasOwnProperty(year)){

            let BatBallData = deliveries.reduce((batsmanData,data)=>{

                if(seasonData[year].has(data.match_id)){

                    if(batsmanData.hasOwnProperty(data.batsman)){

                        batsmanData[data.batsman]["Runs"] += Number(data.batsman_runs);
                        batsmanData[data.batsman]["ball_Faced"] += 1;

                    }
                    else{

                        batsmanData[data.batsman] = { "Runs": Number(data.batsman_runs),
                                                    "ball_Faced":1         
                                                    }
                        
                    }
                    
                }

                return batsmanData

            },{});

        for(batman in BatBallData){
 
                BatBallData[batman] = ((BatBallData[batman].Runs)*100)/BatBallData[batman].ball_Faced

        }
 
        StrikeRateData[year] = BatBallData

        }

        
    }

    return(StrikeRateData)

}

// 8 Find the highest number of times one player has been dismissed by another player
//dismissal_kind
//run out
//player_dismissed
//bowler
const PlayerDismissdByAnotherPlayer = (deliveries)=>{

    let result = deliveries.reduce((dismissedResult,data)=>{

           if(data.player_dismissed ){

            
                              
                if(data.dismissal_kind != 'retired hurt' || data.dismissal_kind != 'obstructing the field'){

                
                    let dismisser = data.dismissal_kind !="run out" ? data.bowler:data.fielder

                    

                    if(dismissedResult.hasOwnProperty(data.player_dismissed)){

                        if(dismissedResult[data.player_dismissed].hasOwnProperty(dismisser)){
                            
                            dismissedResult[data.player_dismissed][dismisser] +=1
                        }
                        else{

                            Object.assign(dismissedResult[data.player_dismissed],{[dismisser]:1})
                        }

                    }
                    else{

                        dismissedResult[data.player_dismissed] = {[dismisser]:1};
                    
                    }
                }
           }

           return dismissedResult

        },{});

        return result

    }     




module.exports = {MatchesPerYear,WinnerTeamYearly,ExtraRunsByTeam,TopTenEconomicalbowlers,NoOfTimesTossAndMatchWins,HighestNumberOfManOfTheMatchPlayer,StrikeRateOfBatsmanBySeason,PlayerDismissdByAnotherPlayer}












